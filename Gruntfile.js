module.exports = function(grunt){
	grunt.initConfig({
		  // ------------------------
            // HTML Build Task
            htmlmin: {
                site: {
                    options: {
                    	collapseWhitespace: true
                    },
                    files: [{
                        expand: true,
                        cwd: './wwwroot/html/',
                        src: 'index.html',
                        dest: './wwwroot/html/lib/'
                    }]
                }
            }
	});

	grunt.loadNpmTasks('grunt-contrib-htmlmin');

	grunt.registerTask('default', ['htmlmin']);
};
﻿/// <binding Clean='clean' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");

var webroot = "./wwwroot/";

var paths = {
    js: webroot + "js/**/*.js",
    minJs: webroot + "js/**/*.min.js",
    css: webroot + "css/**/*.css",
    minCss: webroot + "css/**/*.min.css",
    concatJsDest: webroot + "js/lib/site.min.js",
    concatCssDest: webroot + "css/lib/css/site.min.css",
    destJs: webroot + "js/lib",
    destCss: webroot + "css/lib/css",
	destFonts: webroot + "css/lib/fonts"
};

var jsfilesToMove = [
		webroot + 'lib/jquery/dist/jquery.js',
        webroot + 'lib/bootstrap/dist/js/bootstrap.js',
		webroot + 'lib/animo/animo.js',
		webroot + 'lib/moment/min/moment-with-locales.js'
];

var cssfilesToMove = [
		webroot + 'lib/font-awesome/css/font-awesome.css',
		webroot + 'lib/animate.css/animate.css',
		webroot + 'lib/bootstrap/dist/css/bootstrap.css'
];

var fontsfilesToMove = [
		webroot + 'lib/font-awesome/fonts/*.*',
		webroot + 'lib/bootstrap/dist/fonts/*.*'
];

var jsFilesToConcat = [
	webroot + 'js/lib/jquery.js',
    webroot + 'js/lib/bootstrap.js',
	webroot + 'js/lib/animo.js',
	webroot + 'js/lib/moment-with-locales.js',
	webroot + 'js/site.js'
];

var cssFilesToConcat = [
	webroot + 'css/lib/css/font-awesome.css',
	webroot + 'css/lib/css/animate.css',
	webroot + 'css/lib/css/bootstrap.css',
	webroot + 'css/site.css'
];


gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);


gulp.task("min:jsmove", function() {
	return gulp.src(jsfilesToMove)
			.pipe(gulp.dest(paths.destJs));
});

gulp.task("min:cssmove", function () {
	return gulp.src(cssfilesToMove)
			.pipe(gulp.dest(paths.destCss));
});

gulp.task("min:fontsmove", function () {
	return gulp.src(fontsfilesToMove)
			.pipe(gulp.dest(paths.destFonts));
});

gulp.task("min:js", function () {
    return gulp.src(jsFilesToConcat)
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
	return gulp.src(cssFilesToConcat)
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:jsmove", "min:cssmove", "min:fontsmove", "min:js", "min:css"]);
